----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.03.2016 20:13:00
-- Design Name: 
-- Module Name: TestComplement1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_logic_arith.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TestComplement1 is
--  Port ( );
end TestComplement1;

architecture Behavioral of TestComplement1 is

    component Complement1
    Port ( 
        mot : in STD_LOGIC_VECTOR (3 downto 0);
        sortie : out STD_LOGIC_VECTOR (3 downto 0)
    );
    end component;
    
    signal mot : std_logic_vector (3 downto 0);
    signal sortie : std_logic_vector (3 downto 0);
    
    constant period : time := 10 ns;
begin
    --processus de generation des entrees
    process
    variable val : integer :=0;
    begin
        boucle: FOR val in 0 to 15 loop
            mot<= conv_std_logic_vector(val,4);
            wait for period;
        end loop;
        wait;     
    end process;
    
    -- module complement
    moduleC : Complement1 port map (mot,sortie);

end Behavioral;
